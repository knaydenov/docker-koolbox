FROM golang:1.17 as build

ENV CGO_ENABLED=0 \
    GOOS=linux
ARG KOOLBOX_VERSION=v0.2.0
RUN go install gitlab.com/knaydenov/koolbox@${KOOLBOX_VERSION}



FROM alpine:3.14 as alpine

COPY --from=build /go/bin/koolbox /usr/local/bin/koolbox

RUN addgroup -S koolbox && \
    adduser -S koolbox -G koolbox

USER koolbox
WORKDIR /home/koolbox

COPY ./docker-entrypoint.sh /
ENTRYPOINT [ "/docker-entrypoint.sh" ]
